transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {F:/Desk/Mario/Studio+/Magis/Sistemi Digitali Programmabili - De Leonardis/Quartus Workspace/CarPa_System/CarPa2.vhd}

vcom -93 -work work {F:/Desk/Mario/Studio+/Magis/Sistemi Digitali Programmabili - De Leonardis/Quartus Workspace/CarPa_System/CarPa_testbench2.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  tb

add wave *
view structure
view signals
run -all
