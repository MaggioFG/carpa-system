library IEEE;
use IEEE.std_logic_1164.all;

ENTITY tb_car_parking_system IS
END tb_car_parking_system;
 
ARCHITECTURE behavior OF tb_car_parking_system IS 
 
    -- Component Declaration for the car parking system
 
    COMPONENT CarPa_System
    PORT(
         clock : IN  std_logic;
         reset : IN  std_logic;
         f_sensor : IN  std_logic;
         b_sensor : IN  std_logic;
			pw_1, pw_2, pw_3, pw_4 : IN std_logic; 
         g_led : OUT  std_logic;
         r_led : OUT  std_logic;
			gate : OUT std_logic;
         hex_1 : OUT  std_logic_vector(6 downto 0);
         hex_2 : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal front_sensor : std_logic := '0';
   signal back_sensor : std_logic := '0';
	signal pw_1 : std_logic := '1'; 
	signal pw_2 : std_logic := '0';
	signal pw_3 : std_logic := '0';
	signal pw_4 : std_logic := '1';
	
  --Outputs
   signal GREEN_LED : std_logic;
   signal RED_LED : std_logic;
	signal gate : std_logic;
   signal HEX_1 : std_logic_vector(6 downto 0);
   signal HEX_2 : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
 -- Instantiate the car parking system in VHDL
   Car_park_system: CarPa_System PORT MAP (
          clock => clk,
          reset => reset_n,
          f_sensor => front_sensor,
          b_sensor => back_sensor,
			 pw_1 => pw_1,
			 pw_2 => pw_2,
			 pw_3 => pw_3,
			 pw_4 => pw_4,
          g_led => GREEN_LED,
          r_led => RED_LED,
			 gate => gate,
          hex_1 => HEX_1,
          hex_2 => HEX_2
        );

   -- Clock process definitions
	clk_process :process
   begin
	clk <= '0';
	wait for clk_period/2;
	clk <= '1';
	wait for clk_period/2;
	end process;
   -- Stimulus process
   stim_proc: process
   begin  
      reset_n <= '0';
		front_sensor <= '0';
		back_sensor <= '0';
		wait for clk_period*10;
		reset_n <= '1';
  
		wait for clk_period*10;
		front_sensor <= '1';
		wait for clk_period*10;
		pw_3<='1';
		
      wait until GREEN_LED = '1';
		
		wait for clk_period*3;
			back_sensor <= '1';		-- macchina in mezzo
		wait for clk_period*3;
			front_sensor <= '0';		-- macchina quasi passata
		wait for clk_period*3;
			back_sensor <= '0';		-- macchina entrata/nessuna macchina 
			pw_3 <= '0';
		wait for clk_period *3;
			front_sensor <= '1';
		wait for clk_period *5;
			front_sensor <= '0';
		wait for clk_period *10;
			front_sensor <= '1';
		wait for clk_period *3;
			pw_3 <= '1';
	   wait until GREEN_LED = '1';

		
		
		
      wait;
   end process;

END;
