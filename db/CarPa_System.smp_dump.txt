
State Machine - |CarPa_System|actual_state
Name actual_state.STOP actual_state.RIGHT_PWD actual_state.WRONG_PWD actual_state.WAIT_PWD actual_state.IDLE 
actual_state.IDLE 0 0 0 0 0 
actual_state.WAIT_PWD 0 0 0 1 1 
actual_state.WRONG_PWD 0 0 1 0 1 
actual_state.RIGHT_PWD 0 1 0 0 1 
actual_state.STOP 1 0 0 0 1 

State Machine - |CarPa_System|state
Name state.D state.C state.B state.A 
state.A 0 0 0 0 
state.B 0 0 1 1 
state.C 0 1 0 1 
state.D 1 0 0 1 
