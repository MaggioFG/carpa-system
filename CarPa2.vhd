library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity CarPa_System is
port(
	clock, reset : in std_logic;
	f_sensor, b_sensor : in std_logic;
	pw_1, pw_2, pw_3, pw_4 : in std_logic; 
	g_led, r_led : out std_logic;
	hex_1, hex_2 : out std_logic_vector(6 downto 0);
	gate : out std_logic  -- Output driving the opening or closing of the gate
	);
end CarPa_System;

architecture states of CarPa_System is
--Finite State Machine States
type fsm_states is (IDLE, WAIT_PWD, WRONG_PWD, RIGHT_PWD, STOP);
type state_type is (A,B,C,D);
signal state : state_type := A;
signal actual_state, next_state : fsm_states;
signal red, green : std_logic;

begin

process(actual_state, f_sensor, b_sensor)
begin
	case actual_state is
	when IDLE =>
		if(f_sensor = '1') then 
			next_state <= WAIT_PWD;
		else
			next_state <= IDLE;
		end if;
	when RIGHT_PWD =>
		if(f_sensor = '1' and b_sensor = '1') then
			next_state <= STOP;
		elsif(f_sensor = '1' and  b_sensor = '0') then
			next_state <= RIGHT_PWD;
		else
			next_state <= IDLE;
		end if;
	when STOP =>
		if(f_sensor = '0' and b_sensor = '1') then
			next_state <= IDLE;
		elsif(f_sensor = '1' and b_sensor = '0') then
			next_state <= WAIT_PWD;
		elsif(f_sensor = '1' and b_sensor = '1') then
			next_state <= STOP;
		elsif(f_sensor = '0' and b_sensor = '0') then
			next_state <= IDLE;
		else
			next_state <= IDLE;
		end if;
	when WRONG_PWD =>
		if(f_sensor = '0' and b_sensor = '0') then
			next_state <= IDLE;
		else
			next_state <= WRONG_PWD;
		end if;
	when WAIT_PWD =>
		if(f_sensor = '0' and b_sensor = '0') then
			next_state <= IDLE;
		else
			next_state <= WRONG_PWD;
		end if;
			
	when others => 
		next_state <= IDLE;
	end case;
end process;


process(clock, reset)
	begin
		if( reset = '0') then     --resets state and output signal when reset is asserted.
			state <= A;
			actual_state <= IDLE;
		elsif(rising_edge(clock)) then
			if (next_state = WAIT_PWD or next_state = WRONG_PWD) then
			case state is
					when A =>   --when the current state is A.			 
						if ( pw_1 = '0' ) then
							state <= A;
							actual_state <= next_state;
							--actual_state <= WRONG_PWD;
						else   
							state <= B;
							actual_state <= next_state;
							--actual_state <= WRONG_PWD;
						end if;
					when B =>   --when the current state is B.
						if ( pw_2 = '0' ) then
							state <= C;
							actual_state <= next_state;
							--actual_state <= WRONG_PWD;
						else   
							state <= B;
							actual_state <= next_state;
							--actual_state <= WRONG_PWD;
						end if;
					when C =>   --when the current state is C.
						if ( pw_3 = '0' ) then
							state <= A;
							actual_state <= next_state;
							--actual_state <= WRONG_PWD;
						else   
							state <= D;
							actual_state <= next_state;
							--actual_state <= WRONG_PWD;
						end if;
					when D =>   --when the current state is D.
						if ( pw_4 = '0' ) then
							state <= C;
							actual_state <= WRONG_PWD;
						else   
							state <= A;
							actual_state <= RIGHT_PWD;   --pattern "1011" is found in the sequence.
						end if;    
					when others =>
						NULL;
			end case;
		else
			actual_state <= next_state;
		end if;
	end if;
end process; 

process(clock)
begin
	if(rising_edge(clock)) then
		case(actual_state) is
			when IDLE =>
				green <= '0';
				red <= '0';
				gate <= '0';
				hex_1 <= "1111111";
				hex_2 <= "1111111";
			when WAIT_PWD =>
				green <= '0';
				red <= '1';
				gate <= '0';
				hex_1 <= "0000110"; -- E
				hex_2 <= "0101011"; -- N
			when WRONG_PWD =>
				green <= '0';
				red <= '1';
				gate <= '0';
				hex_1 <= "0000110"; -- E
				hex_2 <= "0000110"; -- E
			when RIGHT_PWD =>
				green <= '1';
				red <= '0';
				gate <= '1';
				hex_1 <= "0000010"; -- 6 := G
				hex_2 <= "1000000"; -- 0
			when STOP =>
				green <= '0';
				red <= '1';
				gate <= '1';
				hex_1 <= "0010010"; -- 5 := S
				hex_2 <= "0001100"; -- P
			when others =>
				green <= '0';
				red <= '0';
				gate <= '0';
				hex_1 <= "1111111"; -- off
				hex_2 <= "1111111"; -- off
		end case;
	end if;
end process;

g_led <= green;
r_led <= red;

end states;

